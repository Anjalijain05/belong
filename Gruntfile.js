module.exports = function( grunt ) {
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		connect: {
    		server: {
      			options: {
        			port: 9000,
        			base: 'src',
        			keepalive:true,
        			middleware : function(connect) {
				        return [
				          connect().use(
				            '/bower_components',
				            connect.static('./bower_components')
				          ),
				          connect.static('./src')
				        ];
				      }
      			}
    		}
  		}
	});

	grunt.loadNpmTasks('grunt-contrib-connect');

	grunt.registerTask('default',['connect']);
};