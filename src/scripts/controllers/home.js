angular.module('belongApp')
  .controller('HomeCtrl', function ($scope,$http,Belong) {
     $scope.belong = new Belong();
     $scope.locations = [];
     $scope.industries = [];
     $http.get('location.json').
        success(function(data) {
            $scope.locations = data;
      });

    $http.get('industry.json').
        success(function(data) {
            $scope.industries = data;
    });
      	
 	$scope.$watchGroup(['belong.selectedLocations','belong.selectedIndustries'], function() {
  		if($scope.belong.initializing){
        $scope.belong.initializing = false;
        return ;
      }
      $scope.belong.filterResults();	
	});


	$scope.getNumber = function(num) {
   	 	return new Array(parseInt(num));   
	}
	$scope.experience = function(value) { return parseInt(value) }
});



  