'use strict';

var myApp = angular
  .module('belongApp', [
    'ngResource',
    'ngRoute',
    'infinite-scroll'
  ]);

  myApp.factory('Belong', function($http) {
  var Belong = function() {
    this.employess = [];
    this.busy = false;
    this.counter = 0;
    this.pageNo =0;
    this.pageSize = 10;
    this.totalEmployess = 0;
    this.selectedLocations = [];
    this.selectedIndustries = [];
    this.minExperience = 0;
    this.maxExperience = 50;
    this.end = false;
    this.initializing = true;
  };

  Belong.prototype.filterResults = function() {
    var locations = [];
    var industries = [];
    for (var i = 0; i < this.selectedLocations.length; i++) {
      locations.push(this.selectedLocations[i].city);
    };
    
    for(var i = 0; i < this.selectedIndustries.length; i++){
      industries.push(this.selectedIndustries[i].profile);
    }
    var url = "employee.json?page_no=" + 0 + "&page_size="+ this.pageSize + "&location=" + locations + "&industry=" + industries ;
    $http.get(url).success(function(data) {
      this.employess = data.employess;
      this.totalEmployess = data.count;
      this.counter = this.employess.length;
      this.pageNo = 0;
    }.bind(this));
  };

  Belong.prototype.nextPage = function() {
    
    if (this.busy  || this.end) return;
    this.busy = true;

    var locations = [];
    var industries = [];
    for (var i = 0; i < this.selectedLocations.length; i++) {
      locations.push(this.selectedLocations[i].city);
    };
    
    for(var i = 0; i < this.selectedIndustries.length; i++){
      industries.push(this.selectedIndustries[i].profile);
    }
    var url = "employee.json?page_no=" + this.pageNo + "&page_size="+ this.pageSize + "&location=" + locations + "&industry=" + industries;

    $http.get(url).success(function(data) {
      var employess = data.employess;
      this.totalEmployess = data.count;
      for (var i = 0; i < employess.length; i++) {
        this.employess.push(employess[i]);
      }
      this.counter = this.counter + employess.length;
      this.pageNo +=1;
      if(this.counter >= this.totalEmployess){
        this.end = true;
      }
      this.busy = false;
    }.bind(this));

   
  };

  return Belong;
});